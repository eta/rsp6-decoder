# rsp6-decoder

This is a set of tools to decode UK National Rail electronic ticket barcodes, specifically RSP6 format,
as explained in the blog post ["Reversing UK mobile rail tickets"](https://eta.st/2023/01/31/rail-tickets.html).

If you're interested in a working demo, check out the hosted version at [eta.st/tickets/](https://eta.st/tickets).

## What's in this repo?

- This repo is primarily a Rust crate that can decode these tickets, given a scanned barcode string (starting with `06...`).
  Look at the `decode_ticket` function for the main entry point.
- A website that embeds the crate for demo purposes (powering the hosted version) is inside `rsp6-webshite/`.
  - This will require compiling the main crate for WASM using `wasm-pack build --features wasm`.
  - If you get errors decoding at runtime, you might need to replace references to `wbindgen_realloc` in
    `pkg/rsp6_decoder_bg.js` with `undefined`. It's unclear why this happens.
- `spec.pdf` has a janky table showing what all the fields in a decoded ticket look like.
  - `spec.ods` is the janky spreadsheet that was used to make this.
- `keys.json` has a copy of all the ticket public keys, as of 2023-01.
  - `decode.py` was a hacky Python script to make these from some binary plist I found at some point. You can ignore it.

## License

This is all MIT. Please go forth and use it to make your own ticket stuff!

## Notes to future eta on how to deploy this

For development:

```
$ npm run start
```

Deploying:

```
$ npm run build -- --mode=production && scp -r dist/* [redacted]:/www/tickets/
```
