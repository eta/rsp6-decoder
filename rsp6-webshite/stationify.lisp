(defun f (x)
  (let ((tr (string-right-trim '(#\Space #\Tab) x)))
    (when (> (length tr) 0)
      tr)))

(with-open-file (f "~/Downloads/CORPUSExtract.json")
  (let* ((cl-json:*json-identifier-name-to-lisp* #'identity)
         (data (cdr (assoc "TIPLOCDATA" (cl-json:decode-json f)
                           :test #'string=)))
         (ret (make-hash-table :test 'equal)))
    (loop
      for entry in data
      do (let ((nlc (cdr (assoc "NLC" entry :test #'string=)))
               (crs (f (cdr (assoc "3ALPHA" entry :test #'string=))))
               (desc (f (cdr (assoc "NLCDESC" entry :test #'string=)))))
           (when (and nlc desc
                      (eql (rem nlc 100) 0))
             (let ((nlc (format nil "~4,'0D" (/ nlc 100))))
               (format t "~A → ~A / ~A~%" nlc crs desc)
               (setf (gethash nlc ret)
                     `(("crs" . ,crs)
                       ("desc" . ,desc)))))))
    ret))
