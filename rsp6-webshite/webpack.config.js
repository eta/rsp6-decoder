const CopyWebpackPlugin = require("copy-webpack-plugin");
let commitHash = require('child_process')
  .execSync('git rev-parse --short HEAD')
  .toString()
  .trim();
const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: "./bootstrap.js",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bootstrap.js",
  },
  mode: "development",
  plugins: [
	  new CopyWebpackPlugin(['index.html', 'govuk.css']),
	  new webpack.DefinePlugin({
		  __COMMIT_HASH__: JSON.stringify(commitHash)
	  })
  ],
};
