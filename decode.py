import ccl_bplist
import json
f = open("/home/eta/Downloads/key_store.archive", "rb")
plist = ccl_bplist.load(f)
ccl_bplist.set_object_converter(ccl_bplist.NSKeyedArchiver_common_objects_convertor)
obj = ccl_bplist.deserialise_NsKeyedArchiver(plist)
ret = dict()
store = obj["store"]
for issuer in store:
    print("issuer: " + issuer)
    issobjs = []
    fobs = store[issuer]["fob"]
    if len(fobs) > 1:
        print("WARNING: Issuer " + issuer + " has " + str(len(fobs)) + " keys!")
    for fob in fobs:
        issobj = dict()
        for key in fob:
            if key == '$class':
                continue
            issobj[key] = fob[key]
        issobjs.append(issobj)
    ret[issuer] = issobjs
f2 = open("./lol.json", "w")
f2.write(json.dumps(ret, indent=4, sort_keys=True, default=str))
f2.close()
