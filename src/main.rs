use rsp6_decoder::decode_ticket;
use rsp6_decoder::keys::IssuerKeyStore;

fn main() {
    let iks = IssuerKeyStore::new();
    eprintln!("Feed in a ticket on stdin (raw scan result starting 06...)");
    let ticket_str = std::io::stdin()
        .lines()
        .next()
        .expect("no ticket provided")
        .expect("error reading ticket");
    match decode_ticket(&iks, &ticket_str) {
        Ok(t) => {
            eprintln!("Decode successful.");
            serde_json::to_writer_pretty(std::io::stdout(), &t).expect("error serializing ticket");
        }
        Err(e) => {
            eprintln!("Failed to decode ticket: {}", e);
        }
    }
}
