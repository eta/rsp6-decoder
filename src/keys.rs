//! Decoding and using the RSA public keys needed to decrypt the ticket data.

use num_bigint::BigUint;
use serde::{Deserialize, Deserializer};
use std::collections::HashMap;

static KEYS_JSON: &'static str = include_str!("../keys.json");

fn deserialize_hex_as_biguint<'de, D>(deserializer: D) -> Result<BigUint, D::Error>
where
    D: Deserializer<'de>,
{
    let buf = String::deserialize(deserializer)?;

    BigUint::parse_bytes(buf.as_bytes(), 16).ok_or(serde::de::Error::custom(format!(
        "failed to parse exponent hex"
    )))
}

#[derive(Deserialize, Debug)]
pub struct IssuerKey {
    pub is_private: bool,
    pub is_test: bool,
    pub issuer_id: String,
    #[serde(
        rename = "modulus_hex",
        deserialize_with = "deserialize_hex_as_biguint"
    )]
    pub modulus: BigUint,
    #[serde(
        rename = "public_exponent_hex",
        deserialize_with = "deserialize_hex_as_biguint"
    )]
    pub public_exponent: BigUint,
    pub public_key_x509: Option<String>,
}

#[derive(Debug)]
pub struct IssuerKeyStore {
    pub keys: HashMap<String, Vec<IssuerKey>>,
}

impl IssuerKeyStore {
    pub fn new() -> Self {
        let keys = serde_json::from_str(KEYS_JSON).expect("failed to parse embedded keys.json");
        Self { keys }
    }

    /*
    pub fn get_key(&self, issuer: &str) -> Vec<RsaPublicKey> {
        let issuer_key = match self.keys.get(issuer) {
            Some(x) => x,
            None => return vec![],
        };
        let modulus = BigUint::from_bytes_be(&issuer_key.modulus);
        let exponent = BigUint::from(issuer_key.public_exponent);
        RsaPublicKey::new(modulus, exponent).expect("failed to parse embedded RSA public key")
    }

     */
}
