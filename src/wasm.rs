//! wasm bindings to run in a browser

use crate::keys::IssuerKeyStore;
use lazy_static::lazy_static;
use std::ops::Deref;
use wasm_bindgen::prelude::wasm_bindgen;
use wasm_bindgen::JsValue;

lazy_static! {
    static ref KEY_STORE: IssuerKeyStore = IssuerKeyStore::new();
}

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

#[wasm_bindgen]
pub fn init() {
    log("[rust] init()");
    log("[rust] setting panic hook");
    std::panic::set_hook(Box::new(console_error_panic_hook::hook));
    log("[rust] loading key store");
    let _store = KEY_STORE.deref();
    log("[rust] init done");
}

#[wasm_bindgen]
pub fn decode_ticket(ticket: String) -> Result<JsValue, String> {
    log(&format!("[rust] decode_ticket called; ticket = {}", ticket));
    let iks = KEY_STORE.deref();
    let ticket = crate::decode_ticket(&iks, &ticket).map_err(|e| e.to_string());
    let ret = serde_wasm_bindgen::to_value(&ticket)
        .map_err(|e| format!("failed to serialise ticket: {}", e))?;
    Ok(ret)
}

#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;
